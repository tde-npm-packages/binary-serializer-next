[hydrator-next](../README.md) / [Exports](../modules.md) / DataConverterInfo

# Interface: DataConverterInfo

## Table of contents

### Properties

- [array](dataconverterinfo.md#array)
- [converter](dataconverterinfo.md#converter)
- [ignore](dataconverterinfo.md#ignore)
- [ignoreDatabase](dataconverterinfo.md#ignoredatabase)
- [ignorePlain](dataconverterinfo.md#ignoreplain)
- [nested](dataconverterinfo.md#nested)
- [parentKey](dataconverterinfo.md#parentkey)
- [translate](dataconverterinfo.md#translate)
- [translateDatabase](dataconverterinfo.md#translatedatabase)
- [translatePlain](dataconverterinfo.md#translateplain)
- [type](dataconverterinfo.md#type)

## Properties

### array

• `Optional` **array**: `boolean`

Indicate that field is an array

___

### converter

• `Optional` **converter**: (`value`: `any`, `direction`: [`DataDirection`](../enums/datadirection.md), `type`: `string`) => `any`

#### Type declaration

▸ (`value`, `direction`, `type`): `any`

Custom converter function

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | value passed to converter |
| `direction` | [`DataDirection`](../enums/datadirection.md) | data direction |
| `type` | `string` | `string` passed as type of field |

##### Returns

`any`

___

### ignore

• `Optional` **ignore**: `boolean` \| ``"dehydrate"`` \| ``"hydrate"``

Value will be ignored when coming or going to database or plain object

* `true` = ignore completely
* `'hydrate'` - when hydrating object with plain data, field will be ignored
* `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object

___

### ignoreDatabase

• `Optional` **ignoreDatabase**: `boolean` \| ``"dehydrate"`` \| ``"hydrate"``

Value will be ignored when coming or going to/from database

* `true` = ignore completely
* `'hydrate'` - when hydrating object with plain data, field will be ignored
* `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object

___

### ignorePlain

• `Optional` **ignorePlain**: `boolean` \| ``"dehydrate"`` \| ``"hydrate"``

Value will be ignored when coming or going to plain object

* `true` = ignore completely
* `'hydrate'` - when hydrating object with plain data, field will be ignored
* `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object

___

### nested

• `Optional` **nested**: `any`

Nested class object type or `forwardRef` of that class object

___

### parentKey

• `Optional` **parentKey**: `string`

Used with conjunction with nested, populates a key of child with current object

**WARNING** This creates circular references

___

### translate

• `Optional` **translate**: `string`

translates key when converting object to plain in any direction

___

### translateDatabase

• `Optional` **translateDatabase**: `string`

translates key when converting object to plain and direction to database

___

### translatePlain

• `Optional` **translatePlain**: `string`

translates key when converting object to plain and direction to plain object

___

### type

• `Optional` **type**: `string`

Type of field, converters has to be registered in order for this field to even work
