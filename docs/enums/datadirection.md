[hydrator-next](../README.md) / [Exports](../modules.md) / DataDirection

# Enumeration: DataDirection

## Table of contents

### Enumeration members

- [FromDatabase](datadirection.md#fromdatabase)
- [FromPlain](datadirection.md#fromplain)
- [ToDatabase](datadirection.md#todatabase)
- [ToPlain](datadirection.md#toplain)

## Enumeration members

### FromDatabase

• **FromDatabase** = `3`

___

### FromPlain

• **FromPlain** = `4`

___

### ToDatabase

• **ToDatabase** = `2`

___

### ToPlain

• **ToPlain** = `1`
