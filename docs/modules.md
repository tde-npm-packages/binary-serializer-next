[hydrator-next](README.md) / Exports

# hydrator-next

## Table of contents

### Enumerations

- [DataDirection](enums/datadirection.md)

### Classes

- [Converter](classes/converter.md)
- [Hydrator](classes/hydrator.md)

### Interfaces

- [DataConverterInfo](interfaces/dataconverterinfo.md)

### Functions

- [FieldType](modules.md#fieldtype)
- [convertBigInt](modules.md#convertbigint)
- [convertNumber](modules.md#convertnumber)
- [convertString](modules.md#convertstring)
- [forwardRef](modules.md#forwardref)

## Functions

### FieldType

▸ **FieldType**(`opts`): (`target`: `any`, `propertyKey`: `string` \| `symbol`) => `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `opts` | [`DataConverterInfo`](interfaces/dataconverterinfo.md) |

#### Returns

`fn`

▸ (`target`, `propertyKey`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `target` | `any` |
| `propertyKey` | `string` \| `symbol` |

##### Returns

`void`

___

### convertBigInt

▸ **convertBigInt**(`value`, `dataDirection`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `dataDirection` | [`DataDirection`](enums/datadirection.md) |

#### Returns

`any`

___

### convertNumber

▸ **convertNumber**(`value`, `dataDirection`): `number`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `dataDirection` | [`DataDirection`](enums/datadirection.md) |

#### Returns

`number`

___

### convertString

▸ **convertString**(`value`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`any`

___

### forwardRef

▸ **forwardRef**<`T`\>(`type`): () => () => `T`

Used when you wish to reference class which does not exist yet or has circular dependencies

**`example`**
```ts
class Car {
	name: string = '';
	@FieldType({ nested: forwardRef(() => CarPrototype) })
	prototype: CarPrototype;
}

class CarPrototype {
	name: string = '';

	@FieldType({ nested: forwardRef(() => Car), array: true })
	cars: Car[];
}
```

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `type` | () => () => `T` | anonymous function returning class type |

#### Returns

`fn`

▸ (): () => `T`

##### Returns

`fn`

• ()
