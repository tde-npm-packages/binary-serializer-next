[hydrator-next](../README.md) / [Exports](../modules.md) / Converter

# Class: Converter

## Table of contents

### Constructors

- [constructor](converter.md#constructor)

### Properties

- [converters](converter.md#converters)

### Methods

- [convert](converter.md#convert)
- [convertSingle](converter.md#convertsingle)
- [registerConverter](converter.md#registerconverter)

## Constructors

### constructor

• **new Converter**()

## Properties

### converters

▪ `Static` **converters**: `Object` = `{}`

#### Index signature

▪ [type: `string`]: (`value`: `any`, `dataDirection`: [`DataDirection`](../enums/datadirection.md)) => `any`

## Methods

### convert

▸ `Static` **convert**(`value`, `info`, `direction`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `info` | [`DataConverterInfo`](../interfaces/dataconverterinfo.md) |
| `direction` | [`DataDirection`](../enums/datadirection.md) |

#### Returns

`any`

___

### convertSingle

▸ `Static` **convertSingle**(`value`, `info`, `direction`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `info` | [`DataConverterInfo`](../interfaces/dataconverterinfo.md) |
| `direction` | [`DataDirection`](../enums/datadirection.md) |

#### Returns

`any`

___

### registerConverter

▸ `Static` **registerConverter**(`type`, `converter`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `type` | `string` |
| `converter` | (`value`: `any`, `dataDirection`: [`DataDirection`](../enums/datadirection.md)) => `any` |

#### Returns

`void`
