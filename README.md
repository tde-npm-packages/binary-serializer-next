# Hydrator Next

---
Next gen Class hydration library, super slim ~1Kb.
Written mostly for Typescript. Works in both node and browser.
While it is possible to use with plain JS it will require some pre-processor like babel with decorators support. 

**Only depends on `reflect-metadata` polyfil.**

For Typescript, you need to have these two options turned on in `tsconfig.json`:

```json
{
	"compilerOptions": {
		"emitDecoratorMetadata": true,
		"experimentalDecorators": true
	}
}
```

![npm](https://img.shields.io/npm/v/hydrator-next)

**WARNING:** API is not yet final

## Requirements

Technically it should have no requirements.

---

## Docs

[Read the docs here](https://gitlab.com/tde-npm-packages/hydrator-next/-/blob/master/docs/modules.md)

---

## Installation

For yarn:

```
yarn add hydrator-next
```

For npm:

```
npm i hydrator-next
```

---

For browser just include the script or import it via ES6 import statement.

## Some examples

---

Imports:

```ts
// ES6 JS/Typescript style
import { Hydrator } from 'hydrator-next';

// require

const { Hydrator } = require('hydrator-next');
```

Basic usage:

```ts
class Test {
	some = 1;
	property = 'a';
}

// hydrate from raw data
const obj = Hydrator.hydrate(Test, {some: 2, property: 'b'});
```

FieldType options (all of them are optional):

| Property | Type | Description |
|----------|------|-------------|
| `type` | `string` | Property type, has to be registered with `Converter.registerConverter` |
| `nested` | `constructor` | If property is another nested class, explicitly state the class, ex. `Date` |
| `array` | `boolean` | Is property an array, need to also provide `type` otherwise it will have no effect |
| `converter` | `(value: any, direction: DataDirection, type: string) => any;` | custom converter function, example of usage below |
| `translate` | `string` | Should this property be translated into some other field when hydrating/dehydrating, useful for mapping plain object `under_score_case` to `camelCaseProperty` |
| `translateDatabase` | `string` | Same as translate however will only have effect when hydrating/dehydrating to database |
| `ignoreDatabase` | `boolean` | value will be ignored when coming or going to database, which basically means field does not exist in database |
| `ignorePlain` | `boolean` | value will be ignored when coming or going to plain object, which basically means field should not be hydrated while operating on raw data |
| `ignore` | `boolean` | value will be ignored, won't ever be touched by Hydrator |

Advanced usage:

```ts
Converter.registerConverter('string', convertString);
Converter.registerConverter('number', convertNumber);
Converter.registerConverter('bigint', convertBigInt);

function customConverter(value: any, direction: DataDirection, type: string) {
	switch (direction) {
		case DataDirection.FromDatabase:
		case DataDirection.FromPlain:
			return '_' + value;
		case DataDirection.ToDatabase:
		case DataDirection.ToPlain:
			return value.substr(1);
	}
}

class TestClassCustom {
	@FieldType({ converter: customConverter })
	cust: any;
	
	// Forced to string upon hydration
	@FieldType({ type: 'string' })
	str: string;
	
	// Forced to number upon hydration
	@FieldType({ type: 'number' })
	num: string;

	// Forced to BigInt upon hydration
	@FieldType({ type: 'bigint' })
	bi: string;
	
	// Any data will come here
	fieldOfAnyValue: any = null;

	// Will not be changed at all upon migration
	@FieldType({ ignore: true })
	ignorethis: string = '9';
}

Hydrator.hydrate(TestClassCustom, {
	cust: 'a',
	str: 123,
	num: '999',
	bi: 123123,
	fieldOfAnyValue: new Date(),
	// field is not decorated nor present as property in class, thus will be ignored.
	willBeIgnored: 'aaa',
	ignorethis: 'nope'
})
```