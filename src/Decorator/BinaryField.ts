import { BinaryFieldInfo } from '../Interface/BinaryFieldInfo';

export function BinaryField(opts: BinaryFieldInfo) {
	return (target: any, propertyKey: string | symbol) => {
		const allMetadata = Reflect.getMetadata('BinaryField', target) || {};
		allMetadata[propertyKey] = opts;

		Reflect.defineMetadata('BinaryField', allMetadata, target);
	};
}