import { BinarySerializer }                                                  from '../src';
import { BinarySerializable }                                                from '../src';
import { BinaryField }                                                       from '../src';
import { BinaryConverter, NumberConverter, FloatConverter, StringConverter } from '../src';
import { toHexString }                                                       from './toHexString';

import isEqual        from 'lodash/isEqual';

BinaryConverter.registerConverter('string', new StringConverter());
BinaryConverter.registerConverter('number', new NumberConverter());
BinaryConverter.registerConverter('float', new FloatConverter());

@BinarySerializable({ layout: 'sequential' })
class TestClass {
	@BinaryField({ type: 'number', size: 4, unsigned: true })
	x = 0xffffffff; //4294967295

	@BinaryField({ type: 'number', size: 4, unsigned: true })
	arraySize = 5; //18446744073709551615

	@BinaryField({ type: 'number', array: true, arraySize: 'arraySize', size: 2 })
	n = [111, 222, 333, 444, 555];

	@BinaryField({ type: 'number', size: 4, unsigned: true })
	y = 12345; //4294967295
}

const t1 = new TestClass();
const out = BinarySerializer.serialize(t1) as number[];
console.log(out);
console.log(toHexString(out as any));

const t1m = BinarySerializer.deserialize(TestClass, out);
console.log(t1);
console.log(t1m);
console.log(isEqual(
	t1,
	t1m
));