import {
	BinaryConverter, BinaryField, BinarySerializable, BinarySerializer, FloatConverter, NumberConverter, StringConverter
} from '../src';

BinaryConverter.registerConverter('string', new StringConverter());
BinaryConverter.registerConverter('number', new NumberConverter());
BinaryConverter.registerConverter('float', new FloatConverter());
@BinarySerializable({ layout: 'sequential' })
export class Ping {
	@BinaryField({ type: 'string', size: 'var' })
	data = 'ping';
	@BinaryField({ type: 'string', size: 'var' })
	data2 = 'abc';
	@BinaryField({ type: 'string', size: 'var' })
	data3 = 'x';
}

console.log(BinarySerializer.serialize(new Ping()));

