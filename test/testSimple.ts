import { BinarySerializer }                                                  from '../src';
import { BinarySerializable }                                                from '../src';
import { BinaryField }                                                       from '../src';
import { BinaryConverter, NumberConverter, FloatConverter, StringConverter } from '../src';
import { Test }                                                              from './Test';
import { toHexString }                                                       from './toHexString';

import isEqual        from 'lodash/isEqual';
import differenceWith from 'lodash/differenceWith';

BinaryConverter.registerConverter('string', new StringConverter());
BinaryConverter.registerConverter('number', new NumberConverter());
BinaryConverter.registerConverter('float', new FloatConverter());

@BinarySerializable({ layout: 'sequential' })
class TestClass {
	@BinaryField({ type: 'number', size: 4, unsigned: true })
	x = 0xffffffff; //4294967295

	@BinaryField({ type: 'number', size: 8, unsigned: true })
	xy = 0xFFFFFFFFFFFFFFFFn; //18446744073709551615

	@BinaryField({ type: 'number', size: 2 })
	vu = -569;

	@BinaryField({ type: 'number', size: 4, unsigned: true })
	v1 = 0xff223344; // 4280431428

	@BinaryField({ type: 'number', size: 2, unsigned: true })
	v2 = 0xFF22; // 65506

	@BinaryField({ type: 'number', size: 8 })
	x64 = 0x0102030405060708n; // 72623859790382848

	@BinaryField({ type: 'string', size: 8, stripNulls: true })
	s = 'test';
}

const t1 = new TestClass();
const out = BinarySerializer.serialize(t1) as number[];
console.log(out);
console.log(toHexString(out as any));

const t1m = BinarySerializer.deserialize(TestClass, out);
console.log(t1, t1m);
console.log(isEqual(
	t1,
	t1m
));
console.log(
	t1.x === t1.x,
	t1.xy === t1.xy,
	t1.vu === t1.vu,
	t1.v1 === t1.v1,
	t1.v2 === t1.v2,
	t1.x64 === t1.x64,
	t1.s === t1.s
);