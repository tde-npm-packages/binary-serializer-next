export class Test {
	static run(name: string, test: (...args: any[]) => boolean | any, ...args: any[]) {
		const res = test(...args);
		let out = (typeof res === 'boolean' && res ? '\x1b[32m✔\x1b[0m' : '\x1b[31m❌\x1b[0m Result: ' + res + ' ');
		out += ' ' + name;
		out += (args.length > 0 ? ' input: ' + args.concat(', ') : '');
		console.log(out);
	}
}