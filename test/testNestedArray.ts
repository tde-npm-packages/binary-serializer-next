import { BinarySerializer }                                                  from '../src';
import { BinarySerializable }                                                from '../src';
import { BinaryField }                                                       from '../src';
import { BinaryConverter, NumberConverter, FloatConverter, StringConverter } from '../src';
import { toHexString }                                                       from './toHexString';

import isEqual from 'lodash/isEqual';

BinaryConverter.registerConverter('string', new StringConverter());
BinaryConverter.registerConverter('number', new NumberConverter());
BinaryConverter.registerConverter('float', new FloatConverter());

@BinarySerializable({ layout: 'sequential' })
class TestNestedArray {
	@BinaryField({ type: 'number', size: 4, unsigned: true })
	x = Math.ceil(Math.random() * 2000);
}

@BinarySerializable({ layout: 'sequential' })
class TestClass {
	@BinaryField({ type: 'number', size: 4, unsigned: true })
	x = 0xffffffff; //4294967295

	@BinaryField({ type: 'number', size: 4, unsigned: true })
	arraySize = 3;

	@BinaryField({ nested: TestNestedArray, array: true, arraySize: 'arraySize' })
	n: TestNestedArray[] = [new TestNestedArray(), new TestNestedArray(), new TestNestedArray()];

	@BinaryField({ type: 'number', size: 4, unsigned: true })
	y = 12345;
}

const t1 = new TestClass();
const out = BinarySerializer.serialize(t1);
console.log(out);
console.log(toHexString(out as any));

const t1m = BinarySerializer.deserialize(TestClass, out as number[]);
console.log('T1', t1);
console.log('T2', t1m);
console.log(isEqual(
	t1,
	t1m
));