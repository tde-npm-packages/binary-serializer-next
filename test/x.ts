import { convertBigInt, BinaryConverter, convertNumber, convertString, DataDirection, FieldType, Hydrator } from '../src';
import { Test }                                                                                             from './Test';

BinaryConverter.registerConverter('string', convertString);
BinaryConverter.registerConverter('number', convertNumber);
BinaryConverter.registerConverter('bigint', convertBigInt);

class Tx {
	a = 1;
	b: number;
}

console.log(new Tx());