import { BinarySerializer }                                                  from '../src';
import { BinarySerializable }                                                from '../src';
import { BinaryField }                                                       from '../src';
import { BinaryConverter, NumberConverter, FloatConverter, StringConverter } from '../src';
import { toHexString }                                                       from './toHexString';
import 'reflect-metadata';

import isEqual from 'lodash/isEqual';

BinaryConverter.registerConverter('string', new StringConverter());
BinaryConverter.registerConverter('number', new NumberConverter());
BinaryConverter.registerConverter('float', new FloatConverter());


@BinarySerializable({
	layout: 'sequential',
	defaultEndian: 'le',
	defaultUnsigned: true,
	defaultType: 'number',
	defaultSize: 4
})
class TestClass {
	x = 0xffffffff; //4294967295

	arraySize = 5;

	@BinaryField({ array: true, arraySize: 'arraySize', size: 2 })
	n = [111, 222, 333, 444, 555];

	y = 12345;
}

const t1 = new TestClass();
const out = BinarySerializer.serialize(t1) as number[];
console.log(out);
console.log(toHexString(out as any));

const t1m = BinarySerializer.deserialize(TestClass, out);
console.log(t1);
console.log(t1m);
console.log(isEqual(
	t1,
	t1m
));