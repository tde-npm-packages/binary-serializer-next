export function toHexString(byteArray: number[]) {
	return byteArray.reduce((output, elem) => (output + ('0' + elem.toString(16)).slice(-2)), '');
}